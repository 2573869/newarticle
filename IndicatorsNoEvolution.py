import pandas as pd
import py2neo as pn
import time
from dateutil import parser
import datetime
import numpy
import matplotlib.pyplot as plt
import os

# count the time of running the program
startGeneral = time.time()
neo4j_url = 'http://localhost:7474/'
user = 'neo4j'
pwd = '0000'
#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph(neo4j_url,  auth=(user, pwd))

#########################################INDICATORS FOR ENTITY CLASSES#########################################

query_nodes ="MATCH (n) " + \
            "WITH n, labels(n) as labels, size(keys(n)) as props, size((n)--()) as degree " +\
            "RETURN "+\
            "DISTINCT labels,"+ \
            "count(*) AS NumofNodes,"+\
            "avg(props) AS AvgNumOfPropPerNode,"+\
            "min(props) AS MinNumPropPerNode,"+\
            "max(props) AS MaxNumPropPerNode,"+\
            "avg(degree) AS AvgNumOfRelationshipsPerNode,"+\
            "min(degree) AS MinNumOfRelationshipsPerNode,"+\
            "max(degree) AS MaxNumOfRelationshipsPerNode,"+\
            "min(n.startvalidtime) as MinStartValidTime,"+\
            "max(n.endvalidtime) as MaxEndValidTime"

#run query
result_query_nodes = graph.run(query_nodes)
print(result_query_nodes)

#record data
result_query_nodes = pd.DataFrame(result_query_nodes, columns=["labels","NumofNodes", "AvgNumOfPropPerNode","MinNumPropPerNode","MaxNumPropPerNode",
                                                                 "AvgNumOfRelationshipsPerNode","MinNumOfRelationshipsPerNode", "MaxNumOfRelationshipsPerNode","MinStartValidTime","MaxEndValidTime"])
path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
result_query_nodes.to_csv(path + '\IndicatorsNoEvolutionNodes.csv', index=False)

#########################################INDICATORS FOR RELATIONSHIP CLASSES#########################################


query_edges = "MATCH ()-[r]-()" +\
              "WITH  r, type(r) as labels, size(keys(r)) as props "+\
              "RETURN "+\
              "DISTINCT labels, "+\
              "count(*) AS NumofEdges, "+\
              "avg(props) AS AvgNumOfPropPerEdge, "+\
              "min(props) AS MinNumPropPerEdge,"+\
              "max(props) AS MaxNumPropPerEdge, "+\
              "min(r.startvalidtime) as MinStartValidTime,"+\
              "max(r.endvalidtime) as MaxEndValidTime"

#run query
result_query_edges = graph.run(query_edges)
print(result_query_edges)
#record data
result_query_edges = pd.DataFrame(result_query_edges, columns=["labels","NumofEdges", "AvgNumOfPropPerEdge","MinNumPropPerEdge","MaxNumPropPerEdge",
                                                                 "MinStartValidTime","MaxEndValidTime"])
path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
result_query_edges.to_csv(path + '\IndicatorsNoEvolutionEdges.csv',  index=False)