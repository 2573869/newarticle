import py2neo as pn
import pandas as pd
import time
import datetime





# count the time of running the program
startGeneral = time.time()
neo4j_url = 'http://localhost:7474/'
user = 'neo4j'
pwd = '0000'
#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph(neo4j_url,  auth=(user, pwd))

# ##################get all the labels of the dataset#################
#https://neo4j.com/blog/data-profiling-holistic-view-neo4j/

#query_labels =  "CALL db.labels()"
#result_query_labels = graph.run(query_labels)
#result_query_labels = result_query_labels.to_series()
#print("Step 1 get all labels: " + result_query_labels)
result_query_labels = ["Student"]



for label in result_query_labels:

    # ##################get the minimum and maximum time in the dataset##################
    query_max_min = "MATCH (n:" + \
                    str(label) + \
                    ")" + \
                    " RETURN min(n.startvalidtime) as min, max(n.endvalidtime) as max"
    result_min_max = graph.run(query_max_min).to_data_frame()
    print("Step 2 get min/max valid time of data : " + result_min_max)

    # ###################create time series of date between min/max time dataset#######
    min = datetime.datetime.fromisoformat(str(result_min_max["min"][0])).date()
    max = datetime.datetime.fromisoformat(str(result_min_max["max"][0])).date()
    print("Etape 3: convert min/max valid time to date: " + str(min) + " " + str(max))
    time_axis = pd.date_range(min, max,freq="5d") #count the number of states of each entity in a period of 5 days
    print("Etape 4: create a time axis: " + str(time_axis))
    #time_axis = [str(i) for i in time_axis]
    #print(time_axis)
    # ##################create csv for recording the indicators#################
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT/"

    newCsv = pd.DataFrame(
        columns=["Startvalidtime","Endvalidtime", "EntityId","NumofStatesPerEntity", "DegreePerEntity"])
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
    newCsv.to_csv(path + "\IndicatorsEvolutionEntities" + str(label) + ".csv", index=False)

    # ###################get the indicators of the dataset#######
    for i in range(0, len(time_axis)-1):
        print(time_axis[i+1])
        t = datetime.datetime.fromisoformat(str(time_axis[i])).date()
        print(t)
        t_after = datetime.datetime.fromisoformat(str(time_axis[i+1])).date()
        print(t_after)
        query_states = "MATCH (n:" + \
                     str(label) + \
                     ")" + \
                      " WHERE date(datetime(n.startvalidtime)) < " + "date('" + str(t_after) + "')" + \
                    " AND date(datetime(n.endvalidtime)) >= " + "date('" + str(t)  +"') "  \
                      " RETURN " + "date('" + str(t) + "') AS Startvalidtime," + \
                       "date('" + str(t_after) + "')" + " AS Endvalidtime," + \
                      "n.entityid as EntityId,"\
                      "count(n.entityid) AS NumofStates"

        print(query_states)
        result_query_states = graph.run(query_states)
        result_query_states = result_query_states.to_data_frame()




        query_degree = "MATCH (n:" + \
                       str(label) + \
                       ")" + "-[r]-()" \
                    " WHERE date(datetime(n.startvalidtime)) < " + "date('" + str(t_after) + "')" + \
                    " AND date(datetime(n.endvalidtime)) >= " + "date('" + str(t) + "') " \
                    "AND date(datetime(r.startvalidtime)) < " + "date('" + str(t_after) + "')" + \
                       " AND date(datetime(r.endvalidtime)) >= " + "date('" + str(
            t) + "') " + \
                        " RETURN " + "date('" + str(t) + "') AS Startvalidtime," + \
                       "date('" + str(t_after) + "')" + " AS Endvalidtime," + \
                       "n.entityid as EntityId," \
                       "count(r) AS DegreeEntity"

        print(query_degree)
        result_query_degree = graph.run(query_degree)
        result_query_degree = result_query_degree.to_data_frame()


        #merge the two results
        result = pd.merge(result_query_states, result_query_degree, how="left", on=["EntityId", "Startvalidtime", "Endvalidtime"])
        #record
        if result_query_states.empty:
            print("no data")
            data = {"Startvalidtime": [str(t)], "Endvalidtime": [str(t_after)], "EntityId": [0],
                    "NumofStatesPerEntity": [0],  "DegreePerEntity": [0]}
            df = pd.DataFrame(data)
            df.to_csv(path + "\IndicatorsEvolutionEntities" + str(label) + ".csv", mode='a', header=False, index=False)
        else:
            print(result)
            result.to_csv(path + "\IndicatorsEvolutionEntities" + str(label) + ".csv", mode='a',
                                       header=False,
                                       index=False)


        # next



