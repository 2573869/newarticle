import py2neo as pn
import pandas as pd
import time
import datetime



# count the time of running the program
startGeneral = time.time()
neo4j_url = 'http://localhost:7474/'
user = 'neo4j'
pwd = '0000'
#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph(neo4j_url,  auth=(user, pwd))

# ##################get all the labels of the dataset#################
#https://neo4j.com/blog/data-profiling-holistic-view-neo4j/

query_labels =  "CALL db.labels()"
result_query_labels = graph.run(query_labels)
result_query_labels = result_query_labels.to_series()
print("Step 1 get all labels: " + result_query_labels)




for label in result_query_labels:

    # ##################get the minimum and maximum time in the dataset##################
    query_max_min = "MATCH (n:" + \
                    str(label) + \
                    ")" + \
                    " RETURN min(n.startvalidtime) as min, max(n.endvalidtime) as max"
    result_min_max = graph.run(query_max_min).to_data_frame()
    print("Step 2 get min/max valid time of data : " + result_min_max)

    # ###################create time series of date between min/max time dataset#######
    min = datetime.datetime.fromisoformat(str(result_min_max["min"][0])).date()
    max = datetime.datetime.fromisoformat(str(result_min_max["max"][0])).date()
    print("Etape 3: convert min/max valid time to date: " + str(min) + " " + str(max))
    time_axis = pd.date_range(min, max,freq="5d")
    print("Etape 4: create a time axis: " + str(time_axis))

    # ##################create csv for recording the indicators#################
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT/"

    newCsv = pd.DataFrame(
        columns=["Time", "NumofNodes", "AvgNumOfPropPerNode", "MinNumPropPerNode", "MaxNumPropPerNode",
                 "AvgNumOfRelationshipsPerNode", "MinNumOfRelationshipsPerNode",
                 "MaxNumOfRelationshipsPerNode"])
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
    newCsv.to_csv(path + "\IndicatorsEvolutionNodes" + str(label) + ".csv", index=False)

    # ###################get the indicators of the dataset#######
    for t in time_axis:
        t = datetime.datetime.fromisoformat(str(t)).date()
        query_nodes = "MATCH (n:" + \
                     str(label) + \
                     ")" + \
                      " WHERE date(datetime(n.startvalidtime)) <= " + "date('" + str(t) + "')" + \
                    " AND (date(datetime(n.endvalidtime)) >= " + "date('" + str(t) + "') OR datetime(n.endvalidtime) IS NULL)" + \
                      " RETURN " + "date('" + str(t) + "')" + " AS Time," +\
                    " count(*) AS NumofNodes," +\
                     "avg(size(keys(n))) AS AvgNumOfPropPerNode," + \
                     "min(size(keys(n))) AS MinNumPropPerNode," + \
                     "max(size(keys(n))) AS MaxNumPropPerNode" + \
                      # "avg(size((n)--())) AS AvgNumOfRelationshipsPerNode," + \
                      #"min(size((n)--())) AS MinNumOfRelationshipsPerNode," + \
                      #"max(size((n)--())) AS MaxNumOfRelationshipsPerNode"
        print(query_nodes)
        result_query_nodes = graph.run(query_nodes)
        result_query_nodes = result_query_nodes.to_data_frame()
        if result_query_nodes.empty:
            print("no data")
            data = {"Time": [str(t)], "NumofNodes":[0], "AvgNumOfPropPerNode":[0], "MinNumPropPerNode":[0], "MaxNumPropPerNode":[0],
                 "AvgNumOfRelationshipsPerNode":[0], "MinNumOfRelationshipsPerNode":[0],
                 "MaxNumOfRelationshipsPerNode":[0]}
            df = pd.DataFrame(data)
            df.to_csv(path + "\IndicatorsEvolutionNodes" + str(label) + ".csv", mode='a', header=False, index=False)
        else:
            print(result_query_nodes)
            result_query_nodes.to_csv(path + "\IndicatorsEvolutionNodes" + str(label) + ".csv", mode='a', header=False,
            index=False)


        # next



# #https://www.codegrepper.com/code-examples/python/python+list+of+dates+between+two+dates
#pd.date_range take dates as inputs




       #
       #
       # query_nodes = "MATCH (n:" + \
       #               str(label) + \
       #               ")" + \
       #                " WHERE date(datetime(n.startvalidtime)) <= " + "date('" + str(t) + "')" + \
       #              " AND date(datetime(n.endvalidtime)) >= " + "date('" + str(t) + "')" + \
       #                " WITH n, size(keys(n)) as props, size((n)--()) as degree " + \
       #                " RETURN " + "date('" + str(t) + "')" + " AS Time," +\
       #              " count(*) AS NumofNodes," +\
       #               "avg(props) AS AvgNumOfPropPerNode," + \
       #               "min(props) AS MinNumPropPerNode," + \
       #               "max(props) AS MaxNumPropPerNode," + \
       #               "avg(degree) AS AvgNumOfRelationshipsPerNode," + \
       #               "min(degree) AS MinNumOfRelationshipsPerNode," + \
       #               "max(degree) AS MaxNumOfRelationshipsPerNode"