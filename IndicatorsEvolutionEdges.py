import py2neo as pn
import pandas as pd
import time
import datetime



# count the time of running the program
startGeneral = time.time()
neo4j_url = 'http://localhost:7474/'
user = 'neo4j'
pwd = '0000'
#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph(neo4j_url,  auth=(user, pwd))

# ##################get all the labels of the dataset#################
#https://neo4j.com/blog/data-profiling-holistic-view-neo4j/
query_labels =  "CALL db.relationshipTypes()"
result_query_labels = graph.run(query_labels)
result_query_labels = result_query_labels.to_series()
print("Step 1 get all labels: " + result_query_labels)




for label in result_query_labels:

    # ##################get the minimum and maximum time in the dataset################## #str(label) + \
    query_max_min = "MATCH ()-[r:" + \
                     str(label) +\
                    "]-()" + \
                    " RETURN min(r.startvalidtime) as min, max(r.endvalidtime) as max"
    result_min_max = graph.run(query_max_min).to_data_frame()
    print("Step 2 get min/max valid time of data : " + result_min_max)

    # ###################create time series of date between min/max time dataset#######
    min = datetime.datetime.fromisoformat(str(result_min_max["min"][0])).date()
    max = datetime.datetime.fromisoformat(str(result_min_max["max"][0])).date()
    print("Etape 3: convert min/max valid time to date: " + str(min) + " " + str(max))
    time_axis = pd.date_range(min, max,freq="1d")
    print("Etape 4: create a time axis: " + str(time_axis))

    # ##################create csv for recording the indicators#################
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT/"

    newCsv = pd.DataFrame(
        columns=["Time", "NumofEdges", "AvgNumOfPropPerEdge","MinNumPropPerEdge","MaxNumPropPerEdge"])
    path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
    newCsv.to_csv(path + "\IndicatorsEvolutionEdges" + str(label) + ".csv", index=False)

    # ###################get the indicators of the dataset#######      #str(label) + \
    for t in time_axis:
        t = datetime.datetime.fromisoformat(str(t)).date()
        query_edges =  "MATCH (n)-[r:" + \
                       str(label)  + \
                       " ]-()" + \
                      " WHERE date(datetime(r.startvalidtime)) <= " + "date('" + str(t) + "')" + \
                    " AND (date(datetime(r.endvalidtime)) >= " + "date('" + str(t) + "') OR datetime(r.endvalidtime) IS NULL)" + \
                      " RETURN " + "date('" + str(t) + "')" + " AS Time," + \
                       "count(*) AS NumofEdges, " + \
                       "avg(size(keys(r))) AS AvgNumOfPropPerEdge, " + \
                       "min(size(keys(r))) AS MinNumPropPerEdge," + \
                       "max(size(keys(r))) AS MaxNumPropPerEdge, " + \
                        "avg(size((n)-[r:" + str(label) + "]-())) AS AvgNumOfRelationshipsPerNode," + \
                        "min(size((n)-[r:" + str(label) + "]-())) AS MinNumOfRelationshipsPerNode," + \
                        "max(size((n)-[r:" + str(label) + "]-())) AS MaxNumOfRelationshipsPerNode"
        print(query_edges)
        result_query_edges = graph.run(query_edges)
        result_query_edges = result_query_edges.to_data_frame()
        if result_query_edges.empty:
            print("no data")
            data = {"Time":[str(t)], "NumofEdges":[0], "AvgNumOfPropPerEdge":[0], "MinNumPropPerEdge":[0], "MaxNumPropPerEdge":[0]}
            df = pd.DataFrame(data)
            df.to_csv(path + "\IndicatorsEvolutionEdges" +  str(label) + ".csv", mode='a', header=False, index=False)
        else :
            print(result_query_edges)
            result_query_edges.to_csv(path + "\IndicatorsEvolutionEdges" + str(label) + ".csv", mode='a', header=False, index=False)

        # next


endGeneral = time.time()
print('time cost', endGeneral - startGeneral, 's')

# #https://www.codegrepper.com/code-examples/python/python+list+of+dates+between+two+dates
#pd.date_range take dates as inputs
MATCH (n)-[r:BlogLiveJournalTwitter]-() WHERE date(datetime(r.startvalidtime)) <= date("2009-01-09") AND (date(datetime(r.endvalidtime)) >= date("2009-01-09") OR datetime(r.endvalidtime) IS NULL) RETURN n LIMIT 10


