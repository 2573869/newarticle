import pandas as pd
import py2neo as pn
import time
from dateutil import parser
import datetime
import numpy
import matplotlib.pyplot as plt
import os

# count the time of running the program
startGeneral = time.time()
neo4j_url = 'http://localhost:7474/'
user = 'neo4j'
pwd = '0000'
#before you have to manually change the active database with respect to the volume you want to query
#in neo4j.conf set dbms.memory.max_heap_size = 4G
# connect DB
graph = pn.Graph(neo4j_url,  auth=(user, pwd))

query_max_min = "MATCH (n:)" + \
                " RETURN min(n.startvalidtime) as min, max(n.endvalidtime) as max"
result_min_max = graph.run(query_max_min).to_data_frame()
print("Step 2 get min/max valid time of data : " + result_min_max)

# ###################create time series of date between min/max time dataset#######
min = datetime.datetime.fromisoformat(str(result_min_max["min"][0])).date()
max = datetime.datetime.fromisoformat(str(result_min_max["max"][0])).date()
print("Etape 3: convert min/max valid time to date: " + str(min) + " " + str(max))
time_axis = pd.date_range(min, max, freq="1d")
print("Etape 4: create a time axis: " + str(time_axis))

#########################################INDICATORS FOR ENTITY CLASSES#########################################

# ##################create csv for recording the indicators#################
path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT/"

newCsv = pd.DataFrame(
    columns=["Time", "NumofNodes", "AvgNumOfPropPerNode", "MinNumPropPerNode", "MaxNumPropPerNode",
             "AvgNumOfRelationshipsPerNode", "MinNumOfRelationshipsPerNode",
             "MaxNumOfRelationshipsPerNode"])
path = "D:/OneDrive - ACTIVUS GROUP/10_Implémentations/dataset/Descriptives statistiques/SOCIAL EXPERIMENT"
newCsv.to_csv(path + "\IndicatorsEvolutionNodes" + str(label) + ".csv", index=False)

# ###################get the indicators of the dataset#######
for t in time_axis:
    t = datetime.datetime.fromisoformat(str(t)).date()
    query_nodes = "MATCH (n)" + \
                  " WHERE date(datetime(n.startvalidtime)) <= " + "date('" + str(t) + "')" + \
                  " AND (date(datetime(n.endvalidtime)) >= " + "date('" + str(
        t) + "') OR datetime(n.endvalidtime) IS NULL)" + \
                  " RETURN " + "date('" + str(t) + "')" + " AS Time," + \
                  " count(*) AS NumofNodes"

    print(query_nodes)
    result_query_nodes = graph.run(query_nodes)
    result_query_nodes = result_query_nodes.to_data_frame()

    query_edges = "MATCH (n)-[r]-()" + \
                  " WHERE date(datetime(r.startvalidtime)) <= " + "date('" + str(t) + "')" + \
                  " AND (date(datetime(r.endvalidtime)) >= " + "date('" + str(
        t) + "') OR datetime(r.endvalidtime) IS NULL)" + \
                  " RETURN " + "date('" + str(t) + "')" + " AS Time," + \
                  "count(*) AS NumofEdges "

    result_query_edges = graph.run(query_edges)
    result_query_edges = result_query_edges.to_data_frame()